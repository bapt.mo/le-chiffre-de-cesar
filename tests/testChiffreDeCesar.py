import unittest
from chiffreur import chiffrer
from dechiffreur import dechiffrer


class TestchiffreDeCesarFunctions(unittest.TestCase):

    def test_chiffrage(self):
       self.assertEqual(chiffrer("abc", 3, False), "def")
       self.assertEqual(chiffrer("XyZ", 3, True), "AbC")
       self.assertEqual(chiffrer("XajYé", 3, True), "AdmBé")

    def test_dechiffrage(self):
       self.assertEqual(dechiffrer("xyz", 3, False), "uvw")
       self.assertEqual(dechiffrer("AbC", 3, True), "XyZ")
       self.assertEqual(dechiffrer("KlaBé", 3, True), "HixYé")