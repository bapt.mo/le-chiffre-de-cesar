from tkinter import * 
from tkinter.messagebox import *
from chiffreur import chiffrer # si chiffreur ou dechiffreur ne sont pas reconnus , enlevez une lettre et remetter la ca résolvera le problème
from dechiffreur import dechiffrer
import string 


frame = Tk()
frame.title("Le Chiffre De César")

frame.resizable(width=False, height=False) # on empêche la redimension de la fenêtre

frame.rowconfigure(4, weight=1) #on configure les lignes
frame.columnconfigure(3, weight=1) #on configure les colonnes

label_saisir = Label(frame , text= "Saisir une phrase: \n") 
label_saisir.grid(column = 0, row = 0)

label_decalage = Label(frame , text = " saisir le décalage \n") 
label_decalage.grid(column = 0 , row =1)

value_chiffreur = StringVar() # declaration de nos variables
value_chiffreur.set("")

value_chiffrage = StringVar()
value_chiffrage.set("")

value_circulaire = BooleanVar()
value_circulaire.set(False)

entry = Entry(frame, textvariable=value_chiffreur, width=30) # textbox où l'utilisateur rentre la chaîne de caractères
entry.grid(column = 1, row = 0)

entry_decalage = Entry(frame , textvariable = value_chiffrage , width = 30) # textbox où l'utilisateur rentre le décalage souhaité
entry_decalage.grid(column= 1 , row =1)

circulaire_checkbox = Checkbutton(frame, text="Chiffrement circulaire", variable = value_circulaire) 
#checkbutton pour savoir si il faut réaliser un chiffrement circulaire ou non
circulaire_checkbox.grid(columnspan= 1, row = 2)

chiffrer_button = Button(frame, text="Chiffrer", command = lambda : showinfo("Chiffré", chiffrer(entry.get(), int(entry_decalage.get()),value_circulaire.get() )))
#boutton qui appelle notre fonction chiffrer , ouvre une nouvelle fenêtre avec le chiffrage
chiffrer_button.grid(column = 0, row = 3)

dechiffrer_button = Button(frame, text="Déchiffrer", command = lambda : showinfo("Déchiffré", dechiffrer(entry.get() ,int(entry_decalage.get()), value_circulaire.get())))
#boutton qui appelle notre fonction déchiffrer , ouvre une nouvelle fenêtre avec le déchiffrage
dechiffrer_button.grid(column = 1, row = 3)

frame.mainloop()
