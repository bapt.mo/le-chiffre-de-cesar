""" 
-cette fonction nous permet de chiffrer une chaîne de caractères
-elle prend deux paramètres en entrée , la chaîne de caractères saisie par l'utilisateur(string)
 et le décalage souhaité par l'utilisateur (string cast en entier)
 elle retourne une chaîne de caractère qui est chiffrée
"""
import string
from tkinter import * 
from tkinter.messagebox import * 

min_letters = string.ascii_lowercase
maj_letters = string.ascii_uppercase

def chiffrer(chiffrage, decalage, circulaire):
     char_list = []
     result_chiffrage = ""

     majuscule = maj_letters[decalage:] + maj_letters[:decalage]
     minuscule = min_letters[decalage:] + min_letters[:decalage]

     if decalage < 1 or decalage >26 :
          return showinfo("Erreur" , "vous devez saisir un nombre entre 1 et 26")

     for parcours in chiffrage :
        
          if parcours == " " :
               char_list.append(" ")

          if circulaire == True :
            
               if (parcours not in min_letters) and (parcours not in maj_letters): #Si parcours est un caractère spécial
                    char_list.append(parcours)         
               else:
                    if parcours.isupper():
                         counter = 0
                         for i in maj_letters:
                              if (parcours == i):
                                   indice = counter
                              counter += 1
                         x = majuscule[indice]
                         char_list.append(x)
                    else:
                         if (parcours.islower()):
                              counter = 0
                              for i in min_letters:
                                   if (parcours == i):
                                        indice = counter
                                   counter = counter + 1
                              x = minuscule[indice]
                              char_list.append(x)
                         else:
                              char_list.append(parcours)
          else:

               char_list.append(chr(ord(parcours) + decalage))
           
     return result_chiffrage.join(char_list) 

